<?php
function smarty_function_ending($params, &$smarty)
{
$base = !isset($params['base']) ? '' : $params['base'];
$e0 = $params['e0'];
$e1 = $params['e1'];
$e2 = $params['e2'];
$number = $params['number'];

if ($number % 100 > 10 && $number % 100 < 20) return $base . $e0;
if ($number % 10 == 1) return $base . $e1;
if ($number % 10 >= 2 && $number % 10 <= 4) return $base . $e2;
return $base . $e0;
}